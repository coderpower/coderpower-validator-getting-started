### Getting started with Validator.js

This first introduction will cover the following functions:

```javascript
    var nameIsAlpha = validator.isAlpha("John");
    var normalizedEmail = validator.normalizeEmail("TeSt@Te.Com");
```

For more information, please refer to the documentation: https://github.com/chriso/validator.js#readme
