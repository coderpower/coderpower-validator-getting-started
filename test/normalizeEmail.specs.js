var chai = require('chai');
var expect = chai.expect;
var validator = require('validator');

var normalizeEmail = require('../sources/normalizeEmail');

describe('Getting started with Validator.js', function() {

    it('The method normalizeEmail() must be invoked', function() {
        var email = "TeSt@Te.Com";
        var expected = validator.normalizeEmail(email);
        var result = normalizeEmail();
        expect(result).to.equal(expected);
    });

});
