var chai = require('chai');
var expect = chai.expect;
var validator = require('validator');

var isAlpha = require('../sources/isAlpha');

describe('Getting started with Validator.js', function() {

    it('The method isAlpha() must be invoked', function() {
        var name = "John";
        var expected = validator.isAlpha(name);
        var result = isAlpha();
        expect(result).to.equal(expected);
    });

});
